# sway patches

## How to apply

1. Clone `sway`
2. Clone this repo
3. Go into the cloned sway repository
4. Run `git apply ../sway-patches/<name>.patch`

```sh
git clone https://github.com/swaywm/sway
git clone https://gitlab.com/alifurkany/sway-patches
cd sway
git apply ../sway-patches/env.patch
```

## License

This project is licensed under the MIT license. See [LICENSE](LICENSE) for more details.

